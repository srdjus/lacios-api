"""Add swipe column to answers

Revision ID: bab0d72c8d53
Revises: 2a43f8817adb
Create Date: 2022-09-17 23:24:33.674859

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'bab0d72c8d53'
down_revision = '2a43f8817adb'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('answers', sa.Column('swipe', sa.Integer(), nullable=True))
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('answers', 'swipe')
    # ### end Alembic commands ###
