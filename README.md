# Lacios-API

## About
The API was created for an interactive people-meeting application, where people answer each other's questions in real-time. Two individuals connect or send requests to each other, and then a room is created. Once both people enter the room, the game begins. The questions are loaded from a database, along with the possible answers. Users pick questions for their partner, partner responds, and the users choose whether they like the answer or not.

After the game, the percentage of matching answers is displayed, and there is an option to continue chatting.

## Stack
The technologies used to build the API are:
- FastAPI,
- SQLAlchemy ORM (Postgres),
- Alembic for migrations,
- and Websockets.

Features:
- User sessions,
- Authentication,
- Google OAuth2
- ORM models and CRUD (via Pydantic).

The documentation for the API is available at `localhost/docs`.


## Running 
Before running API, questions and answers should be inserted into the database.
```
uvicorn --host 0.0.0.0 src.main:app --reload
```

