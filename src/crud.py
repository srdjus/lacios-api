"""DB operations"""
from sqlalchemy.orm import Session
from sqlalchemy.sql.expression import func
from sqlalchemy import or_
from passlib.context import CryptContext
from .utils import random_init_username

from . import models, schemas

# TODO: Handle exceptions
# TODO: Add secret
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


class NativeUserExist(Exception):
    """Raised when user tries to sign in using external
    providers but (native) user with same e-mail exists"
    """


def get_users(db: Session):
    """Returns all users (temporary, no need for this)"""
    return db.query(models.User).all()


def get_user(db: Session, user_id: int):
    """Returns user based on user id"""
    return (
        db.query(models.User)
        .filter(models.User.user_id == user_id)
        .limit(1)
        .first()
    )


def get_user_by_username(db: Session, username: str):
    """Returns user based on username"""
    return (
        db.query(models.User)
        .filter(models.User.username == username)
        .limit(1)
        .first()
    )


def get_user_by_email(db: Session, email: str):
    """Returns user based on email"""
    return (
        db.query(models.User)
        .filter(models.User.email == email)
        .limit(1)
        .first()
    )


def create_user(db: Session, user: schemas.UserCreate):
    """User registration, returns created user"""
    hashed_password = pwd_context.hash(user.password)

    db_user = models.User(
        username=user.username,
        hashed_password=hashed_password,
        email=user.email,
    )
    db.add(db_user)
    db.commit()
    db.refresh(db_user)

    return db_user


def sign_in_google_user(db: Session, id_token: dict):
    """Signs in user using Google OAuth2. There are couple scenarios.
    If user (with such email) does not exist in db, create user and set
    external: true with adequate sub id.
    If user exist and it is already signed in using Google services,
    then return data.
    If user exist and it is signed up using native sign up method,
    raise Exception.

    Arguments:
    id_token - parsed (and verified) Google ID token holding necessary data.

    Returns:
    user - schemas.User
    """

    # Get user by email
    user = get_user_by_email(db, id_token["email"])

    # First login
    if user is None:
        # Check if username exist and if does generate
        # new one until we have unique one
        generated_username = random_init_username()

        while (
            db.query(models.User)
            .filter(models.User.username == generated_username)
            .limit(1)
            .first()
            is not None
        ):
            generated_username = random_init_username()

        db_user = models.User(
            username=generated_username,
            email=id_token["email"],
            external_type=schemas.ExternalTypeEnum.google,
            external_id=id_token["sub"],
        )

        db.add(db_user)
        db.commit()

        return db_user

    if user.external_type == "native":
        # Can't log in using Google Account already used with native login
        raise NativeUserExist

    # Don't really sure if needed to check sub field
    return user


def update_user_info(
    db: Session, current_user: schemas.User, updated_info: schemas.UserUpdate
):
    """Updates user info/profile"""
    db.query(models.User).filter(
        models.User.user_id == current_user.user_id
    ).update(updated_info.dict(exclude_unset=True), synchronize_session=False)
    db.commit()

    return updated_info.dict(exclude_unset=True)


def check_credentials(db: Session, user: schemas.UserLogin):
    """Check user password and return user if correct"""
    db_user = (
        db.query(models.User)
        .filter(models.User.username == user.username)
        .first()
    )

    if not db_user:
        return None

    if pwd_context.verify(user.password, db_user.hashed_password):
        return db_user

    return None


def get_invites(db: Session, user_id: int, expired: bool = False):
    """Returns both sent an>d received invites based on user_id."""
    return (
        db.query(models.Invite)
        .filter(
            models.Invite.expired == expired,
            or_(
                models.Invite.sender_id == user_id,
                models.Invite.receiver_id == user_id,
            ),
        )
        .all()
    )


def get_invite_history(db: Session, user_id: int):
    """Reads expired invites form the database"""
    return (
        db.query(models.Invite)
        .filter(
            models.Invite.expired.is_(True),
            or_(
                models.Invite.sender_id == user_id,
                models.Invite.receiver_id == user_id,
            ),
        )
        .all()
    )


def get_invite(db: Session, invite_id: int):
    """Returns invite based on invite id"""
    return (
        db.query(models.Invite)
        .filter(models.Invite.invite_id == invite_id)
        .first()
    )


def create_invite(db: Session, sender_id: int, receiver_id: int):
    """Creates an invite based on sender_id and receiver_id"""
    db_invite = models.Invite(sender_id=sender_id, receiver_id=receiver_id)
    db.add(db_invite)
    db.commit()
    db.refresh(db_invite)

    return db_invite


def close_invite(
    db: Session, invite_id: int, answers: list[schemas.AnswerCreate]
):
    """Marks an invite as expired"""
    db.query(models.Invite).filter(
        models.Invite.invite_id == invite_id
    ).update({models.Invite.expired: True}, synchronize_session=False)

    for answer in answers:
        db_answer = models.Answer(**answer.dict())
        db.add(db_answer)

    db.commit()

    return True


def get_questions(db: Session):
    """Return 6, random, questions."""
    return db.query(models.Question).order_by(func.random()).limit(6).all()
