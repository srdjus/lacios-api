"""Basic functions used by script, such as dependencies,
parsers and similar.
"""
import string
import random
from datetime import datetime, timedelta
from functools import lru_cache
from fastapi import Depends, HTTPException, WebSocket, status
from fastapi.security import OAuth2PasswordBearer
from sqlalchemy.orm import Session
from jose import JWTError, jwt
from src.config import Settings
from .database import SessionLocal
from . import schemas, crud

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


@lru_cache
def get_settings():
    """Running once, .env variables are cached."""
    return Settings()


def get_db():
    """Database session dependency"""
    db = SessionLocal()

    try:
        yield db
    finally:
        db.close()


def create_access_token(data: dict, expires_delta: timedelta | None = None):
    """
    Creating a JWT after successful login
    Args:
    data -- is a dictionary with "sub" key which is user id
        (by definition in JWT)
    expires_delta -- time span in milisceonds, session duration
    Returns:
    encoded_jwt -- token value
    """
    to_encode = data.copy()

    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=60)

    # Adding expire date to dict
    to_encode.update({"exp": expire})

    settings = get_settings()

    # Hashing dictionary and converting it to token string
    return jwt.encode(
        to_encode, key=settings.secret_key, algorithm=settings.algorithm
    )


def get_current_user(
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(get_db),
    settings: Settings = Depends(get_settings),
) -> schemas.User:
    # Repeating status code response
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )

    # TODO: Pass settings as dependency if it makes sense?

    try:
        # Parsing data from token
        payload = jwt.decode(
            token, settings.secret_key, algorithms=[settings.algorithm]
        )

        username = payload.get("sub")

        # Username is not in the dict, no session
        if username is None:
            raise credentials_exception

    except JWTError:
        raise credentials_exception

    user: schemas.User = crud.get_user_by_username(db, username)

    # No user with "username"
    if user is None:
        raise credentials_exception

    return user


def get_current_active_user(
    current_user: schemas.User = Depends(get_current_user),
):
    """Checking if user account is active"""

    if current_user.is_active:
        return current_user

    raise HTTPException(
        status_code=status.HTTP_400_BAD_REQUEST, detail="Inactive account."
    )


def get_current_socket_user(
    websocket: WebSocket,
    db: Session = Depends(get_db),
    settings: Settings = Depends(get_settings),
):
    """
    Temporary solution, because OAuth dependency does not work with websockets.
    Reading the access_token from header, parsing it and returning user data.
    """
    token = websocket.headers.get("cookie")

    if token is None:
        # TODO: In the future there will be WebSocketException
        # https://github.com/encode/starlette/pull/527
        return None

    try:
        # Parsing data from token
        payload = jwt.decode(
            token, settings.secret_key, algorithms=[settings.algorithm]
        )

        username = payload.get("sub")

        # Username is not in the dict, no session
        if username is None:
            return None

    except JWTError:
        return None

    user: schemas.User = crud.get_user_by_username(db, username)

    # No user with "username"
    if user is None or not user.is_active:
        return None

    return user


def random_init_username():
    """Generates random username, 6 alphanumeric letters + 4 numbers."""
    return "".join(
        [
            *random.choices(string.ascii_lowercase, k=6),
            *random.choices(string.digits, k=4),
        ]
    )
