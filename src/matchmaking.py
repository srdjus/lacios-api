"""Matchmaking classes, implemented through websockets"""
from fastapi import WebSocket
from sqlalchemy.orm import Session
from . import schemas, crud


class SocketRoom:
    """Invite room. Creates when one of two users
    connect to the socket. One room per invite.
    """

    # TODO: Find a better way to keep websocket and user data
    # TODO: Move answers to separate class
    # TODO: Split this into more classes, too many attributes here

    def __init__(
        self,
        invite_id: int,
        sender_id: int,
        receiver_id: int,
        questions: list[schemas.Question],
    ):
        self.invite_id: str = invite_id
        self.sender_id: int = sender_id
        self.receiver_id: int = receiver_id
        self.connections: dict[str, WebSocket] = {
            "sender": None,
            "receiver": None,
        }
        self.is_ready: bool = False
        self.questions: list[schemas.Question] = questions
        self.answers: list[schemas.AnswerCreate] = []
        self.current_question_index: int = -1
        self.game_started: bool = False

    async def add_user(self, connection: WebSocket, user_id: int):
        """Adds user to the 'room'. Room has up to 2 users/sockets."""
        if user_id == self.sender_id:
            self.connections["sender"] = connection
        elif user_id == self.receiver_id:
            self.connections["receiver"] = connection
        else:
            # TODO: Raise exception
            return

        if (
            self.connections["sender"] is not None
            and self.connections["receiver"] is not None
        ):
            # Both users are in, game can start
            for key, conn in self.connections.items():
                await conn.send_json(
                    [
                        "USER_JOINED",
                        {
                            "response": "BEGIN",
                            "players": [
                                {"username": "player"},
                                {"username": "player"},
                            ],  # TODO: Attach user data to socket
                            "player_index": 0 if key == "sender" else 1,
                        },
                    ]
                )

                self.game_started = True  # At this stage the invite is "used"
        else:
            # Waiting for partner
            await connection.send_json(
                ["USER_JOINED", {"response": "WAITING"}]
            )

    async def parse_message(self, websocket: WebSocket, code: str, data: dict):
        """Parses message sent over socket, and generates adequate answer."""
        # TODO: Move codes and responses to separate module
        # (for example game class)
        if code == "NOTIFY_READY":
            await websocket.send_json(
                [
                    "GAME_READY",
                    {
                        "availableQuestions": [
                            self.questions[0].dict(),
                            self.questions[1].dict(),
                        ]
                    },
                ]
            )
        elif code == "QUESTION_PICKED":
            self.current_question_index = len(self.answers) * 2 + data["index"]

            # Sending chosen question to the partner
            await self.send_to_partner(
                websocket,
                [
                    "QUESTION_READY",
                    {
                        "picked_index": data["index"],
                        "question": self.questions[
                            self.current_question_index
                        ].dict(),
                    },
                ],
            )
        elif code == "QUESTION_ANSWERED":
            # TODO: Catch validation exception for AnswerCreate
            self.answers.append(
                schemas.AnswerCreate(
                    question_id=self.questions[
                        self.current_question_index
                    ].question_id,
                    user_id=self.sender_id
                    if websocket == self.connections["sender"]
                    else self.receiver_id,
                    choice_id=data["choice_id"],
                    invite_id=self.invite_id,
                    swipe=0,
                )
            )

            # Send answer to the partner
            await self.send_to_partner(
                websocket,
                ["PARTNER_ANSWERED", {"choice_id": data["choice_id"]}],
            )
        elif code == "ANSWER_SWIPED":
            # -1 - dislike, 1 - like
            # TODO: Catch validation exception for swip field
            self.answers[-1].swipe = data["index"]

            if len(self.answers) >= 3:
                # One of the users will receive questions/answers
                # after last swipe. Another user gets them after
                # turn over event.
                # TODO: Send things back to the user, run out of questions
                await self.send_to_both(
                    [
                        "GAME_OVER",
                        {
                            "questions": [
                                question.dict() for question in self.questions
                            ],
                            "answers": [
                                answer.dict() for answer in self.answers
                            ],
                        },
                    ],
                )
            else:
                await self.send_to_partner(
                    websocket,
                    ["PARTNER_SWIPED", {"swipeIndex": data["index"]}],
                )
        elif code == "TURN_OVER":
            await self.send_to_both(
                [
                    "NEW_QUESTIONS",
                    {
                        "availableQuestions": [
                            self.questions[len(self.answers) * 2].dict(),
                            self.questions[len(self.answers) * 2 + 1].dict(),
                        ]
                    },
                ]
            )
        elif code == "CHAT_ACCEPTED":
            await self.send_to_partner(websocket, ["CHAT_ACCEPTED", {}])
        elif code == "GOT_MESSAGE":
            await self.send_to_partner(
                websocket, ["GOT_MESSAGE", {"body": data["body"]}]
            )

    async def send_to_partner(self, websocket: WebSocket, message: list):
        """Sends message to room partner."""
        for _, conn in self.connections.items():
            if conn != websocket:
                await conn.send_json(message)

    async def send_to_both(self, message: list):
        """Sends message to both users in the room."""
        for _, conn in self.connections.items():
            await conn.send_json(message)

    async def disconnect(self, websocket: WebSocket, db: Session):
        """When user leaves the room there are two possible cases:
        1. user left and the other user was not connected -> removing room
            from the memory (handled by socket manager)
        2. user left and the other user was connected
            (game_started set to true) -> removing from memory
            and updating invite data in the database
        """

        # User disconnected after game has already started
        # NOTE: Answers are saved only after invite sender
        # leaves the room (to prevent double insertions)
        if self.game_started and websocket == self.connections["sender"]:
            crud.close_invite(
                db, invite_id=self.invite_id, answers=self.answers
            )


class ConnectionManager:
    def __init__(self):
        self.rooms: list[SocketRoom] = []

    def add_room(self, room: SocketRoom):
        self.rooms.append(room)

    async def send_self(self, message: str, websocket: WebSocket):
        """Sends a message to the back to the user."""
        await websocket.send_text(message)

    async def send_room(self, message: str, invite_id: int):
        """Sends a message to both users in the room"""
        # TODO: Find a better way to do this, it is ineffective to search
        # for users after each command
        # TODO: Check if websocket belongs to the room
        room = next(
            (room for room in self.rooms if room.id == invite_id), None
        )

        if room:
            for websocket in room.connections:
                await websocket.send_text(message)

    def remove_room(self, room: SocketRoom):
        try:
            print(f"Removing room, number of rooms: {len(self.rooms)}")
            self.rooms.remove(room)
            print(f"Room removed, number of rooms: {len(self.rooms)}")
        except ValueError:
            # TODO: Explore this case
            print("Error removing the room.")


class Manager:
    """
    TODO: Handle removal from waiting list after user disconnects from the ws.
    """

    def __init__(self):
        self.waiting = []

    async def join(
        self, websocket: WebSocket, filters: dict, db: Session, user_id: int
    ):
        """Handles user join.
        Loop through available partners and see if
        they're compatibile.
        """

        for client in self.waiting:
            if filters.get("tag") != client["filters"].get("tag"):
                continue

            # TODO: sender_id and receiver_id make no sense here
            invite = crud.create_invite(db, user_id, client["user_id"])

            await websocket.send_json(
                ["GAME_FOUND", {"invite_id": invite.invite_id}]
            )
            await client["websocket"].send_json(
                ["GAME_FOUND", {"invite_id": invite.invite_id}]
            )

            self.waiting.remove(client)

            return

        self.waiting.append(
            {"websocket": websocket, "filters": filters, "user_id": user_id}
        )

    def disconnect(self, websocket):
        for i, soc in enumerate(self.waiting):
            if soc["websocket"] == websocket:
                del self.waiting[i]
                break
