"""SQLAlchemy model definitions"""
from sqlalchemy import (
    Column,
    Integer,
    String,
    Boolean,
    ForeignKey,
    DateTime,
    Enum,
    Table,
    ForeignKeyConstraint,
    UniqueConstraint,
)
from sqlalchemy.orm import relationship
from sqlalchemy.sql.expression import func

from .database import Base

from .schemas import GenderEnum, ExternalTypeEnum


# TODO: Take a look at this, lazy loading needed!
# https://docs.sqlalchemy.org/en/14/orm/loading_relationships.html
class Invite(Base):
    __tablename__ = "invites"

    invite_id = Column(Integer, primary_key=True)

    sender_id = Column(Integer, ForeignKey("users.user_id"))
    receiver_id = Column(Integer, ForeignKey("users.user_id"))
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    expired = Column(Boolean, default=False)

    sender = relationship(
        "User", foreign_keys="Invite.sender_id", back_populates="invites_sent"
    )

    receiver = relationship(
        "User",
        foreign_keys="Invite.receiver_id",
        back_populates="invites_received",
    )

    answers = relationship("Answer")


class User(Base):
    __tablename__ = "users"

    user_id = Column(Integer, primary_key=True)
    username = Column(
        String,
        unique=True,
    )
    email = Column(String, unique=True)
    avatar = Column(Integer, default=4)
    hashed_password = Column(String)
    description = Column(String)
    location = Column(String)
    is_active = Column(Boolean, default=False)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    gender = Column(Enum(GenderEnum))
    age = Column(Integer)
    external_type = Column(
        Enum(ExternalTypeEnum), default=ExternalTypeEnum.native
    )
    external_id = Column(String)

    invites_sent = relationship("Invite", foreign_keys="Invite.sender_id")
    invites_received = relationship(
        "Invite", foreign_keys="Invite.receiver_id"
    )


# Question - Choice (many to many)
questionschoices_table = Table(
    "questionschoices",
    Base.metadata,
    Column("question_id", ForeignKey("questions.question_id")),
    Column("choice_id", ForeignKey("choices.choice_id")),
    # TODO: For some reason PrimaryKeyConstraint does create constraint in DB
    UniqueConstraint("question_id", "choice_id"),
)


class Question(Base):
    __tablename__ = "questions"

    question_id = Column(Integer, primary_key=True)
    body = Column(String)
    category = Column(String)

    choices = relationship("Choice", secondary=questionschoices_table)


class Choice(Base):
    __tablename__ = "choices"

    choice_id = Column(Integer, primary_key=True)
    body = Column(String)


class Answer(Base):
    __tablename__ = "answers"

    __table_args__ = (
        ForeignKeyConstraint(
            ["question_id", "choice_id"],
            ["questionschoices.question_id", "questionschoices.choice_id"],
        ),
    )
    answer_id = Column(Integer, primary_key=True)
    invite_id = Column(Integer, ForeignKey("invites.invite_id"))
    user_id = Column(Integer, ForeignKey("users.user_id"))
    question_id = Column(Integer)
    choice_id = Column(Integer)
    swipe = Column(Integer)

    question = relationship(
        "Question",
        # This arguments are passed because composite
        # FK was for question_id and choice_id!
        primaryjoin="Question.question_id == Answer.question_id",
        foreign_keys=question_id,
    )

    user = relationship("User")
