"""questionschoices unique field

Revision ID: 60d1710e2b95
Revises: 9f5c8028dd11
Create Date: 2022-09-04 15:39:08.460211

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '60d1710e2b95'
down_revision = '9f5c8028dd11'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint('answers_choice_id_fkey', 'answers', type_='foreignkey')
    op.drop_constraint('answers_question_id_fkey', 'answers', type_='foreignkey')
    op.create_foreign_key(None, 'answers', 'questionschoices', ['question_id', 'choice_id'], ['question_id', 'choice_id'])
    op.alter_column('questionschoices', 'question_id',
               existing_type=sa.INTEGER(),
               nullable=True)
    op.alter_column('questionschoices', 'choice_id',
               existing_type=sa.INTEGER(),
               nullable=True)
    op.create_unique_constraint(None, 'questionschoices', ['question_id', 'choice_id'])
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'questionschoices', type_='unique')
    op.alter_column('questionschoices', 'choice_id',
               existing_type=sa.INTEGER(),
               nullable=False)
    op.alter_column('questionschoices', 'question_id',
               existing_type=sa.INTEGER(),
               nullable=False)
    op.drop_constraint(None, 'answers', type_='foreignkey')
    op.create_foreign_key('answers_question_id_fkey', 'answers', 'questions', ['question_id'], ['question_id'])
    op.create_foreign_key('answers_choice_id_fkey', 'answers', 'choices', ['choice_id'], ['choice_id'])
    # ### end Alembic commands ###
