"""Working with config files"""
from pydantic import BaseSettings


class Settings(BaseSettings):
    """Defines Settings model/schema"""
    secret_key: str
    algorithm: str
    access_token_expires_minutes: int

    db_url: str

    client_id: str

    class Config:
        """env file definition"""
        env_file = ".env"
