"""Pydantic schemas and generic classes"""
from datetime import datetime
from enum import Enum
from typing import ForwardRef, Optional
from pydantic import BaseModel


class GenderEnum(str, Enum):
    """Simple gender enumeration"""
    male = "male"
    female = "female"


class ExternalTypeEnum(str, Enum):
    """External system definitions enumeration"""
    native = "native"
    google = "google"


class UserBase(BaseModel):
    username: str


class UserLogin(UserBase):
    password: str


class UserCreate(UserLogin):
    email: str


class UserEmbed(UserBase):
    """User class used inside of Invite class
    (to prevent recursive/circular calls)
    """

    username: str

    class Config:
        orm_mode = True


class UserUpdate(BaseModel):
    description: Optional[str]
    gender: Optional[GenderEnum]
    location: Optional[str]
    age: Optional[int]
    avatar: Optional[int]


class User(UserBase):
    user_id: int
    is_active: bool
    description: Optional[str]
    location: Optional[str]
    created_at: datetime
    age: Optional[int]
    gender: Optional[GenderEnum]
    avatar: Optional[int]

    invites_sent: list[ForwardRef("Invite")]
    invites_received: list[ForwardRef("Invite")]

    class Config:
        orm_mode = True


class Choice(BaseModel):
    choice_id: int
    body: str

    class Config:
        orm_mode = True


class Question(BaseModel):
    question_id: int
    body: str
    category: str
    choices: list[Choice]

    class Config:
        orm_mode = True


class AnswerCreate(BaseModel):
    invite_id: int
    user_id: int
    question_id: int
    choice_id: int
    swipe: int


class Answer(BaseModel):
    choice_id: int
    question: Question
    swipe: int
    user: UserEmbed

    class Config:
        orm_mode = True


class InviteBase(BaseModel):
    sender_id: int
    receiver_id: int


class Invite(BaseModel):
    invite_id: int
    sender: UserEmbed
    receiver: UserEmbed
    created_at: datetime
    expired: bool

    class Config:
        orm_mode = True


class InviteHistory(Invite):
    answers: list[Answer]


User.update_forward_refs()


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: str | None = None
