"""Main server script"""
from datetime import timedelta
from fastapi import (
    FastAPI,
    Depends,
    Body,
    Form,
    Header,
    WebSocket,
    WebSocketDisconnect,
    HTTPException,
    status,
)
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session
from google.auth.transport import requests
from google.oauth2 import id_token

from src.config import Settings

from . import crud, models, schemas
from . import matchmaking as mm
from .database import engine
from .utils import (
    get_db,
    get_settings,
    get_current_user,
    get_current_socket_user,
    get_current_active_user,
    create_access_token,
)

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

connection_manager = mm.ConnectionManager()
matchmaking_manager = mm.Manager()


@app.get("/")
def get_root():
    """API root, no special function."""
    return {"message": "Welcome to Lacios API!"}


@app.post("/token/")
def login_for_access_token(
    form_data: OAuth2PasswordRequestForm = Depends(),
    db: Session = Depends(get_db),
    settings: Settings = Depends(get_settings),
):
    """Login route, parses form data and returns
    JWT token together with user data
    """
    bad_request_exception = HTTPException(
        status_code=status.HTTP_400_BAD_REQUEST,
        detail="Incorrect username or password",
    )

    user = crud.check_credentials(
        db,
        schemas.UserLogin(
            username=form_data.username, password=form_data.password
        ),
    )

    if user is None:
        raise bad_request_exception

    access_token_expires = timedelta(
        minutes=settings.access_token_expires_minutes
    )

    # Successful loggin, create token
    access_token = create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )

    return {
        "access_token": access_token,
        "token_type": "bearer",
        # TODO: Return only required fields, no need to
        # keep all user data in the session (client)
        "user": schemas.User.from_orm(user),
    }


@app.post("/oauth2token/")
def login_for_token_oauth2(
    token: str = Form(),
    db: Session = Depends(get_db),
    settings: Settings = Depends(get_settings),
):
    """Authenticate user logged via Google OAuth2 by verifyng ID token"""
    try:
        idinfo = id_token.verify_oauth2_token(
            token, requests.Request(), settings.client_id
        )

        # ID token is valid
        user = crud.sign_in_google_user(db, idinfo)

        if user is None:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Existing native account",
            )

        access_token_expires = timedelta(
            minutes=settings.access_token_expires_minutes
        )

        # Successful loggin, create token
        access_token = create_access_token(
            data={"sub": user.username}, expires_delta=access_token_expires
        )

        return {
            "access_token": access_token,
            "token_type": "bearer",
            # TODO: Return only required fields, no need
            # to keep all user data in the session (client)
            "user": schemas.User.from_orm(user),
        }

    except crud.NativeUserExist as exc:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Native user exists",
        ) from exc
    except ValueError as exc:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid token"
        ) from exc


@app.get("/users/", response_model=schemas.User)
def read_users(
    db: Session = Depends(get_db),
    username: str | None = None,
    email: str | None = None,
):
    """Returns single user, based on username or email"""

    if username:
        return crud.get_user_by_username(db, username)

    if email:
        return crud.get_user_by_email(db, email)

    raise HTTPException(
        status_code=status.HTTP_400_BAD_REQUEST,
        detail="No parameteres passed.",
    )


@app.post("/users/", response_model=schemas.User)
def create_user(user: schemas.UserCreate, db: Session = Depends(get_db)):
    """Registration route (native registration), returns created user."""
    db_user = crud.get_user_by_email(db, user.email)

    if db_user:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="E-mail already registered.",
        )

    db_user = crud.get_user_by_username(db, user.username)

    if db_user:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Username already registered.",
        )

    return crud.create_user(db, user)


@app.get("/users/me/", response_model=schemas.User)
def read_user_me(
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(get_current_user),
):
    """Returns the current user"""
    return crud.get_user_by_username(db, current_user.username)


@app.get("/users/{username}/", response_model=schemas.User)
def read_user(username: str, db: Session = Depends(get_db)):
    return crud.get_user_by_username(db, username)


@app.put("/users/me/")
def update_user(
    user: schemas.UserUpdate,
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(get_current_active_user),
):
    return crud.update_user_info(db, current_user, user)


@app.post(
    "/invites/",
    response_model=schemas.Invite,
)
def create_invite(
    receiver_id: int = Body(embed=True),
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(get_current_active_user),
):
    return crud.create_invite(
        db, sender_id=current_user.user_id, receiver_id=receiver_id
    )


@app.get("/invites/", response_model=list[schemas.Invite])
def read_invites(
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(get_current_active_user),
):
    """Get sent and received invites for session user."""
    return crud.get_invites(db, current_user.user_id)


@app.get("/invites/history/", response_model=list[schemas.InviteHistory])
def read_invites_history(
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(get_current_active_user),
):
    """Get invite history for session user."""
    result = crud.get_invite_history(db, current_user.user_id)

    print(result)
    return result


@app.get("/questions/", response_model=list[schemas.Question])
def read_questions(db: Session = Depends(get_db)):
    """For testing purposes. Route will be removed."""
    db_questions = crud.get_questions(db)

    print(db_questions)

    return db_questions


@app.websocket("/invites/{invite_id}/ws/")
async def websocket_invite(
    websocket: WebSocket,
    invite_id: int,
    socket_user: schemas.User = Depends(get_current_socket_user),
    # current_user: schemas.User = Depends(get_current_active_user),
    db: Session = Depends(get_db),  # Is keeping db connection open good idea?
):
    await websocket.accept()

    if socket_user is None:
        await websocket.close()

    # Check if socket room already exist
    room = next(
        (
            room
            for room in connection_manager.rooms
            if room.invite_id == invite_id
        ),
        None,
    )

    invite_db = crud.get_invite(db, invite_id)  # TODO: Validate invite_id

    if room:
        # Room already exist add user to it
        await room.add_user(websocket, socket_user.user_id)
    else:
        # Get questions
        db_questions = crud.get_questions(db)

        # Convert list of questions to list of pydantic models
        questions = [
            schemas.Question.from_orm(db_question)
            for db_question in db_questions
        ]

        # Create a room and add a user
        room = mm.SocketRoom(
            invite_id, invite_db.sender_id, invite_db.receiver_id, questions
        )
        await room.add_user(websocket, socket_user.user_id)

        # Add room to manager
        connection_manager.add_room(room)

    try:
        while True:
            data = await websocket.receive_json()

            # Wrap this inside of try / catch and create event schema
            await room.parse_message(websocket, code=data[0], data=data[1])

    except WebSocketDisconnect:
        print("Websocketclient disconnected.")
        await room.disconnect(websocket, db=db)

        # Remove from memory
        connection_manager.remove_room(room)


@app.websocket("/matchmaking/")
async def websocket_matchmaking(
    websocket: WebSocket,
    tag: str | None = Header(default=None),
    socket_user: schemas.User = Depends(get_current_socket_user),
    db: Session = Depends(get_db),
):
    await websocket.accept()

    # Move this to dependecy, and return exception
    if socket_user is None:
        await websocket.close()

    filters = {}

    if tag is not None:
        filters["tag"] = tag

    # TODO: Parse user data and filters>
    await matchmaking_manager.join(websocket, filters, db, socket_user.user_id)

    try:
        while True:
            await websocket.receive_text()
    except WebSocketDisconnect:
        # TODO: Remove user from matchmakers.waiting room if in it
        print("User disconnected.")
        matchmaking_manager.disconnect(websocket)
